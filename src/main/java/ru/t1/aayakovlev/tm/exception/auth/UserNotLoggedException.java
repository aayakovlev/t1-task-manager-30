package ru.t1.aayakovlev.tm.exception.auth;

public final class UserNotLoggedException extends AbstractAuthException {

    public UserNotLoggedException() {
        super("Error! You are not logged in...");
    }

}
