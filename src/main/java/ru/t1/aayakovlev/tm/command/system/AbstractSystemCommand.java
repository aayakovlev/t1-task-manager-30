package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.service.CommandService;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected CommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
