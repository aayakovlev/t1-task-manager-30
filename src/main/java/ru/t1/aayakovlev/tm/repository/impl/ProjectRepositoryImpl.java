package ru.t1.aayakovlev.tm.repository.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

public final class ProjectRepositoryImpl extends AbstractUserOwnedRepository<Project> implements ProjectRepository {

    @Override
    @NotNull
    public Project create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        return save(userId, project);
    }

    @Override
    @NotNull
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return save(userId, project);
    }

}
