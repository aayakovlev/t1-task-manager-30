package ru.t1.aayakovlev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.service.PropertyService;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(@NotNull final PropertyService propertyService, @NotNull final String value) {
        @NotNull String result = value;
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                @NotNull final String hex = Integer.toHexString((b & 0xFF) | 0x100);
                sb.append(hex, 1, 3);
            }
            return sb.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
