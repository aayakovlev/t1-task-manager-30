package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface SaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
