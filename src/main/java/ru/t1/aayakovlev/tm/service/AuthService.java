package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.model.User;

public interface AuthService {

    void checkRoles(Role[] roles) throws AbstractException;

    @NotNull
    User getUser() throws AbstractException;

    @NotNull
    String getUserId() throws UserNotLoggedException;

    boolean isAuth();

    void login(@Nullable final String login, @Nullable final String password) throws AbstractException;

    void logout();

    @NotNull
    User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException;

}
